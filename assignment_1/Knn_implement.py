import sys
import math
#Use the heapq implementation to efficiently compute the n largest elements
from heapq import nlargest

#Function to find the song with the highest ID
def maxsongid():
	test_file = open('song_mapping.txt', 'r')
	input_list = test_file.readlines()
	user_len = len(input_list)
	max=0
	for line in range(0,user_len):
		t=input_list[line].split('\t')
		if(int(t[0])>max):
			max=int(t[0])
	test_file.close()
	return max

#Function to get all the users from the training data
def find_users():
	user_train = open('user_train.txt', 'r')
	input_list = user_train.readlines()
	user_train.close()
	return  input_list

#Function to print menu - to make the main code look simpler!!
def print_menu():
	print ("\n Data input usage \n")
	print ("Enter 0 for artist input if you want to use KNN for a given user")
	print ("To select the algo used for calculating the similarity metrics input selection for index:")
	print ("1 -> Inverse euclidean distance")
	print ("2 - > Dot product")
	print ("3 - > Vector cosine function\n")

	print ("To select weighted enter 1 , or 0 for unweighted ranking calculation\n")

##The Similarity metric functions
#These guys will return tuples with (x, user_id) pairs

#1. Inverse of the eucledian distance
def euclid_dist_inverse(u, user_count, max_song_id, ud):
	#euclidean distance dictionary
	ed = {}
	
	for i in range (user_count):
		similarity_metric = 0
		denominator = 0
		if i != u:
			for j in range (0, max_song_id):
				similarity_metric += math.pow((int(ud[u][j]) - int(ud[i][j])),2)
			denominator = math.sqrt(similarity_metric)
			if denominator != 0:
				similarity_metric=1/denominator
			else:
				similarity_metric = 0
			ed[i] = similarity_metric
		else:
			ed[i] = 0
	
	#converting the dictionary to list of tuples - similarity metric list for
	#all the users    
	metric_list = [(v,k) for k, v in ed.items()]
	return metric_list
		
#2. Dot product
def dot_product(u, user_count, max_song_id, ud):
	#dot product dictionary
	dp = {}

	for i in range (user_count):
		similarity_metric = 0
		if i != u:
			for j in range (0, max_song_id):
				similarity_metric += float(ud[u][j]) * float(ud[i][j])
			dp[i] = similarity_metric
		else:
			dp[i] = 0

	#converting the dictionary to list of tuples - similarity metric list for 
	#all the users	
	metric_list = [(v,k) for k, v in dp.items()]
	return metric_list

#3. Cosine function 
def cosine_func(u, user_count, max_song_id, ud):
	#cosine function dictionary
	cf = {}
	
	for i in range (user_count):
		similarity_metric = 0
		a=0
		b=0
		c=0
		if i != u:
			for j in range (0, max_song_id):
				#similarity_metric += 1/(math.sqrt(math.pow((float(ud[u][j]) - float(ud[i][j])), 2)))
				a+=math.pow((int(ud[u][j])), 2)
				b+=math.pow((int(ud[i][j])), 2)
				c+=int(ud[u][j]) * int(ud[i][j])
			similarity_metric=c/(math.sqrt(a)*math.sqrt(b))
			cf[i] = similarity_metric
		else:
			cf[i] = 0
	
	metric_list = [(v,k) for k, v in cf.items()]
	return metric_list

#Functions to calculate ranking
#1. weighted
def rank_weighted(k, item_not_present, ud, knn):
	#weighted 
	#calculate the summation of x for KNN
	#iterate through for each song not present in the playlist
	print("calculating ranking in weighted case")
	song_ranking = {}
	for i in range(len(item_not_present)):
		numerator_sum = 0
		denominator_sum = 0
		#iterate through each of the KNN and sum up the metric values for the songs not present
		for j in range(int(k)):
			numerator_sum += float(ud[knn[j][1]][item_not_present[i]]) * float(knn[j][0])
			denominator_sum += float(knn[j][0])
		ranking = numerator_sum/denominator_sum;
		song_ranking[i] = ranking
	
	#converting the dictionary to list of tuples - similarity metric list for 
	#all the users	
	metric_list = [(v,k) for k, v in song_ranking.items()]
	return metric_list

#2. unweighted
def rank_unweighted(k, item_not_present, ud, knn):
	#unweighted 
	#calculate the summation of x for KNN
	#iterate through for each song not present in the playlist
	print("calculating ranking in unweighted case")
	song_ranking = {}
	for i in range(len(item_not_present)):
		metric_sum = 0
		#iterate through each of the KNN and sum up the metric values for the songs not present
		for j in range(int(k)):
			metric_sum += float(ud[knn[j][1]][item_not_present[i]])
		ranking = metric_sum/float(k);
		song_ranking[i] = ranking
	
	#converting the dictionary to list of tuples - similarity metric list for 
	#all the users	
	metric_list = [(v,k) for k, v in song_ranking.items()]
	return metric_list

#Function to construct the dummy user test vector given a non-default value
#for the artist name
def create_user_list_artist(artist, max_song_id):
	print("create user list for artist query")
	list_vector_freq = []
	
	test_file = open('song_mapping.txt', 'r')

	#Parse through the song_mapping and get all the artist matching songs
	#and artists. Store it in a fake user data with 1 as the played value
	list_vector_freq = [0 for i in range (max_song_id)]
	for line in test_file.readlines():
		t=line.split('\t')
		if line.find(artist) != -1:
			list_vector_freq[int(t[0])] = 1
	
	test_file.close()
	return list_vector_freq

def display_10(list_freq_vector):
	top_10_dict = {}
	top_10_list = []

	top_10_dict = dict(enumerate(list_freq_vector))
		
	print ("user's top 10 songs")
	
	top_10_list = [(v,k) for k, v in top_10_dict.items()]
	
	#enumerate the song ids for the user
	top_10 =  nlargest(10, top_10_list, key=lambda e:e[0])

	#Display the songs
	display_songs(top_10)	

def display_songs(song_list):
	filep = open("song_mapping.txt", 'r')

	for line in filep.readlines():
		for i in range(0, len(song_list)):
			t=line.split('\t')
			if int(t[0]) == song_list[i][1]:
				print (line)
	filep.close()

def calculate_precision_10(u, top_10):
	print ("precision")
	filep = open("user_test.txt", 'r')
	
	t = []

	for i, line in enumerate(filep):
		count = 0
		if i == u-1:
			t = line.split(" ")
			t[-1] = t[-1].strip()
	for j in range(10):
		for k in range(2, len(t)):
			print (t[k])
			if top_10[j] == t[k]:
				count += 1
	print (count)
	print ("Precision at 10 is")
	print (count/10)

def main_func():
	#get the number of users in the training data
	training_data = find_users()

	#The list that stores the song frequency pairs of all the
	#users
	user_song_freq = []

	#list of the songs and the frequencies stored for each user
	list_vector_freq = []

	#maximum number of songs in the list
	max_song_id = maxsongid()

	#Get the song and the played pairs from the training data for each user
	for line in training_data:
		temp = line.split(' ')
		#strip out the \n in the end - NOT SURE
		temp[-1] = temp[-1].strip()
		user_song_freq.append(temp)

	#No of users in the list
	user_count = len(training_data)

	#List of user objects
	ud = [0 for i in range (user_count)]

	#scan through the training data and store only the songs and frequencies
	#for each user
	#TODO: modify this to optimize - store sparse vector
	for i in range(user_count):
		list_vector_freq = [0 for k in range (max_song_id)]
		for j in range (2, len(user_song_freq[i])):
			temp = user_song_freq[i][j].split(':')
			
			#temp[0] has the song id, temp[1] has the freq
			list_vector_freq[int(temp[0])] = int(temp[1])
		ud[i] = list_vector_freq

	#Calculate the similarity metrics

	#Print the instructions
	print_menu()

	#Getting the user input
	#First get the input for the artist query to decide if we need to 
	#go ahead with the normal KNN or construct a dummy user for the artist list
	artist = input ("Enter the name of an artist you want to query:")
	if artist == "0":
		u = eval(input("enter the value of u:" ))
		u = u - 1
	else:
		u = 0


	k = input("enter the value of k:")
	i_similarity = input("enter the index to calculate similarity metrics:")
	weighted = input("weighted or unweighted :")

	artist_list = []
	#Go through this part if there is a non-default entry for the artist
	if u == 0:
		print ("calculate the fake user vector for the artist queried!")
		artist_list = create_user_list_artist(artist, max_song_id)
		ud.append(artist_list)
		u = user_count
		user_count += 1
	print (u)
	#Display the top 10 songs for the user
	display_10(ud[u])
	
	#test code to check data parsing - to be removed
	#test_file = open('test.dat', 'w')

	print (" calculating the similarity metrics!")

	if i_similarity == "1":
		#Inverse euclidean distance
		metric_list = euclid_dist_inverse(u, user_count, max_song_id, ud)
	elif i_similarity == "2": 
		#dot product
		metric_list = dot_product(u, user_count, max_song_id, ud)
	elif i_similarity == "3":
		#Cosine function
		metric_list = cosine_func(u, user_count, max_song_id, ud)
	else:
		sys.exit("Enter the right input you fool!!")

	print (" calculating the K-NNs!")
	#test print
	#print (metric_list)

	#Find the K closest neighbours from the metric list
	knn = nlargest(int(k), metric_list, key=lambda e:e[0])

	#test print
	print (knn)

	#Now rank the songs played 0 in the users playlist wrt his KNN
	#find all those indexes for which the value of played is 0
	item_not_present = []
	for i in range(max_song_id):
		if ud[u][i] == 0:
			item_not_present.append(i)

	#temp print to be removed
	#print (item_not_present)

	#for each song not in the playlist, calculate the rank
	print ("calculating the ranking!")

	if weighted == "0":
		metric_list = rank_unweighted(k, item_not_present, ud, knn)
	elif weighted == "1":
		metric_list = rank_weighted(k, item_not_present, ud, knn)
	else:
		sys.exit("Enter the right input you fool!!")

	#Find the top 10 songs
	top_10 =  nlargest(10, metric_list, key=lambda e:e[0])
	
	print (top_10)

	#Display the top 10 songs for the user
	display_songs(top_10)
	
	#Find the precision@10
	print (u)
	calculate_precision_10(u, top_10)

main_func()
