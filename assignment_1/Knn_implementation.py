import sys
import math
import random

#Use the heapq implementation to efficiently compute the n largest elements
from heapq import nlargest

#Function to find the song with the highest ID
def maxsongid():
	song_list = []
	test_file = open('song_mapping.txt', 'r')
	input_list = test_file.readlines()
	user_len = len(input_list)
	for line in range(user_len):
		t = input_list[line].split('\t')
		song_list.append(int(t[0]))
	test_file.close()
	return max(song_list)

#Function to get all the users from the training data
def find_users():
	user_train = open('user_train.txt', 'r')
	input_list = user_train.readlines()
	user_train.close()
	return  input_list

#Function to print menu - to make the main code look simpler!!
def print_menu():
	print("\n Data input usage \n")
	print("Enter 0 for artist input if you want to use KNN for a given user")
	print("To select the algo used for calculating the similarity metrics input selection for index:")
	print("1 -> Inverse euclidean distance")
	print("2 - > Dot product")
	print("3 - > Vector cosine function\n")

	print("To select weighted enter 1 , or 0 for unweighted ranking calculation\n")

##The Similarity metric functions
#These guys will return tuples with (x, user_id) pairs

#1. Inverse of the eucledian distance
def euclid_dist_inverse(u, user_count, max_song_id, ud):
	#euclidean distance dictionary
	ed = {}
	
	for i in range (user_count):
		similarity_metric = 0
		denominator = 0
		x_sq = 0

		#Checking to make sure that the same users data is not used to
		#calculate the similarity metrics
		if i != u:
			for j in range (0, max_song_id):
				x_sq += math.pow((int(ud[u][j]) - int(ud[i][j])),2)
			denominator = math.sqrt(x_sq)
			if denominator == 0:
				similarity_metric = 0
			else:
				similarity_metric=1/denominator
			ed[i] = similarity_metric
		else:
			ed[i] = 0
	
	#converting the dictionary to list of tuples - similarity metric list for
	#all the users    
	metric_list = [(v,k) for k, v in ed.items()]
	return metric_list
		
#2. Dot product
def dot_product(u, user_count, max_song_id, ud):
	#dot product dictionary
	dp = {}

	for i in range (user_count):
		similarity_metric = 0
		
		#Checking to make sure that the same users data is not used to
		#calculate the similarity metrics
		if i != u:
			for j in range (0, max_song_id):
				similarity_metric += float(ud[u][j]) * float(ud[i][j])
			dp[i] = similarity_metric
		else:
			dp[i] = 0

	#converting the dictionary to list of tuples - similarity metric list for 
	#all the users	
	metric_list = [(v,k) for k, v in dp.items()]
	return metric_list

#3. Cosine function 
def cosine_func(u, user_count, max_song_id, ud):
	#cosine function dictionary
	cf = {}
	
	for i in range (user_count):
		similarity_metric = 0
		x_sq=0
		y_sq=0
		xy=0
		
		#Checking to make sure that the same users data is not used to
		#calculate the similarity metrics
		if i != u:
			for j in range (0, max_song_id):
				x_sq+=math.pow((int(ud[u][j])), 2)
				y_sq+=math.pow((int(ud[i][j])), 2)
				xy+=int(ud[u][j]) * int(ud[i][j])
			similarity_metric=xy/(math.sqrt(x_sq)*math.sqrt(y_sq))
			cf[i] = similarity_metric
		else:
			cf[i] = 0
	
	metric_list = [(v,k) for k, v in cf.items()]
	return metric_list

#Functions to calculate ranking
#1. weighted
def rank_weighted(k, max_song_id, item_present, ud, knn):
	#weighted 
	#calculate the summation of x for KNN
	#iterate through for each song not present in the playlist
	print("\ncalculating ranking in weighted case")
	song_ranking = {}

	for i in range(max_song_id):
		numerator_sum = 0
		denominator_sum = 0
		#iterate through each of the KNN and sum up the metric values for the songs not present
		for j in range(int(k)):
			numerator_sum += int(ud[knn[j][1]][i]) * float(knn[j][0])
			denominator_sum += float(knn[j][0])
		if denominator_sum == 0:
			ranking = 0
		else:
			ranking = numerator_sum/denominator_sum;
		song_ranking[i] = ranking

	#Remove the items that are already present in users playlist	
	for i in range(len(item_present)):
		del song_ranking[item_present[i]]
	
	#converting the dictionary to list of tuples - similarity metric list for 
	#all the users	
	metric_list = [(v,k) for k, v in song_ranking.items()]
	return metric_list

#2. unweighted
def rank_unweighted(k, max_song_id, item_present, ud, knn):
	#unweighted 
	#calculate the summation of x for KNN
	#iterate through for each song not present in the playlist
	print("\ncalculating ranking in unweighted case")
	song_ranking = {}
	for i in range(max_song_id):
		metric_sum = 0
		#iterate through each of the KNN and sum up the metric values for the songs not present
		for j in range(int(k)):
			metric_sum += int(ud[knn[j][1]][i])
		ranking = metric_sum/float(k);
		song_ranking[i] = ranking
	
	#Remove the items that are already present in users playlist	
	for i in range(len(item_present)):
		del song_ranking[item_present[i]]
	
	#converting the dictionary to list of tuples - similarity metric list for 
	#all the users	
	metric_list = [(v,k) for k, v in song_ranking.items()]
	return metric_list

#Function to construct the dummy user test vector given a non-default value
#for the artist name
def create_user_list_artist(artist, max_song_id):
	print("\ncreate user list for artist query")
	list_vector_freq = []
	
	test_file = open('song_mapping.txt', 'r')

	#Parse through the song_mapping and get all the artist matching songs
	#and artists. Store it in a fake user data with 1 as the played value
	list_vector_freq = [0 for i in range (max_song_id)]
	for line in test_file.readlines():
		t=line.split('\t')
		if line.find(artist) != -1:
			list_vector_freq[int(t[0])] = 1
	
	test_file.close()
	return list_vector_freq

#Function to get the top 10 songs
def display_10(list_freq_vector):
	top_10_dict = {}
	top_10_list = []

	top_10_dict = dict(enumerate(list_freq_vector))
		
	print("\nuser's top 10 songs")
	
	top_10_list = [(v,k) for k, v in top_10_dict.items()]
	
	#enumerate the song ids for the user
	top_10 =  nlargest(10, top_10_list, key=lambda e:e[0])

	print("\nRanking of the songs")
	for i in range(len(top_10)):
		print(i+1, top_10[i][1])

	#Display the songs
	display_songs_double(top_10)	

#Function to display the songs given a 1D list
def display_songs_single(song_list, count):
	filep = open("song_mapping.txt", 'r')
	
	print("\nSong list:")
	for line in filep.readlines():
		for i in range(0, count):
			t=line.split('\t')
			if int(t[0]) == song_list[i]:
				print(line)
	filep.close()

#Function to display the songs given a 2D list
def display_songs_double(song_list):
	filep = open("song_mapping.txt", 'r')
	
	print("\nSong list:")
	for line in filep.readlines():
		for i in range(0, len(song_list)):
			t=line.split('\t')
			if int(t[0]) == song_list[i][1]:
				print(line)
	filep.close()

#Function to calculate the precision@10
def calculate_precision_10(u, top_10):
	filep = open("user_test.txt", 'r')
	
	t = []
	count = 0

	for i, line in enumerate(filep):
		if i == u:
			t = line.split(" ")
			t[-1] = t[-1].strip()
	for j in range(10):
		for k in range(2, len(t)):
			if int(top_10[j][1]) == int(t[k]):
				count = count + 1
	print("\nPrecision at 10 is")
	print(count/10)

#Main Function : Code begins here
def main_func():
	#Flag to enable and disable the sections of code to run or not
	flag = 1

	#get the number of users in the training data
	training_data = find_users()

	#The list that stores the song frequency pairs of all the
	#users
	user_song_freq = []

	#list of the songs and the frequencies stored for each user
	list_vector_freq = []

	#maximum number of songs in the list
	max_song_id = maxsongid() + 1

	#Get the song and the played pairs from the training data for each user
	for line in training_data:
		temp = line.split(' ')
		#strip out the \n in the end - NOT SURE
		temp[-1] = temp[-1].strip()
		user_song_freq.append(temp)

	#No of users in the list
	user_count = len(training_data)

	#List of user objects
	ud = [0 for i in range (user_count)]

	#scan through the training data and store only the songs and frequencies
	#for each user
	for i in range(user_count):
		list_vector_freq = [0 for k in range (max_song_id)]
		for j in range (2, len(user_song_freq[i])):
			temp = user_song_freq[i][j].split(':')
			
			#temp[0] has the song id, temp[1] has the freq
			list_vector_freq[int(temp[0])] = int(temp[1])
		ud[i] = list_vector_freq

	#Calculate the similarity metrics

	#Print the instructions
	print_menu()

	#Getting the user input
	
	#First get the input for the artist query to decide if we need to 
	#go ahead with the normal KNN or construct a dummy user for the artist list
	artist = input ("Enter the name of an artist you want to query:")
	if artist == "0":
		u = eval(input("enter the value of u:" ))
	else:
		u = 0


	k = input("enter the value of k:")
	i_similarity = input("enter the index to calculate similarity metrics:")
	weighted = input("weighted or unweighted :")

	artist_list = []
	#Go through this part if there is a non-default entry for the artist
	if u == 0:
		artist_list = create_user_list_artist(artist, max_song_id)
		ud.append(artist_list)
		user_count += 1
		u = user_count

	#We need to do this since we store users from 0 to max-user_id
	u = u - 1
	
	#Display the top 10 songs listened by the user
	display_10(ud[u])
	
	#Similarity Metrics
	print(" calculating the similarity metrics!")

	if i_similarity == "1":
		#Inverse euclidean distance
		metric_list = euclid_dist_inverse(u, user_count, max_song_id, ud)
	elif i_similarity == "2": 
		#dot product
		metric_list = dot_product(u, user_count, max_song_id, ud)
	elif i_similarity == "3":
		#Cosine function
		metric_list = cosine_func(u, user_count, max_song_id, ud)
	else:
		sys.exit("Enter the right input for the similarity metric index!!")

	print("\ncalculating the K-NNs!")

	#Find the K closest neighbours from the metric list
	knn = nlargest(int(k), metric_list, key=lambda e:e[0])

	#Now rank the songs played 0 in the users playlist wrt his KNN
	#find all those indexes for which the value of played is 0
	item_present = []
	for i in range(max_song_id):
		if ud[u][i] != 0:
			item_present.append(i)

	#for each song not in the playlist, calculate the rank
	print("\ncalculating the ranking!")

	if weighted == "0":
		metric_list = rank_unweighted(int(k), max_song_id, item_present, ud, knn)
	elif weighted == "1":
		metric_list = rank_weighted(int(k), max_song_id, item_present, ud, knn)
	else:
		sys.exit("Enter the right input for weighted or un-weighted selection!!")
	
	#Find the top 10 songs
	top_10 =  nlargest(10, metric_list, key=lambda e:e[0])
	
	#Print the top-10 recommended songs
	print("\nTop 10 recommended songs for the user:")
	for i in range(len(top_10)):
		print(i+1, top_10[i][1])

	#Display the top 10 songs for the user
	display_songs_double(top_10)
	
	#Find the precision@10
	calculate_precision_10(u, top_10)

	if flag == 1:
		#Random Baseline for comparison
		random_list = []
		for i in range(max_song_id):
			random_list.append(i)
		random.shuffle(random_list)
		print("\nRandomly generated list\n")
		for i in range(10):
			print(i+1, random_list[i])
		display_songs_single(random_list, 10)

		#Popularity baseline
		song_list = [0 for i in range(max_song_id)]
		song_dict = {}
		for i in range(max_song_id):
			user_song_sum = 0
			for j in range(user_count):
				user_song_sum += int(ud[j][i])
			song_list[i] = user_song_sum
	
		song_dict = dict(enumerate(song_list))
		song_list = [(v,k) for k, v in song_dict.items()]
	
		#enumerate the song ids for the user
		song_10 =  nlargest(10, song_list, key=lambda e:e[0])

		print("\nTop 10 songs based on popularity")
		for i in range(len(song_10)):
			print(i+1, song_10[i][1])
		display_songs_double(song_10)

	
main_func()	
