from subprocess import call
import math
import random

t1=open("articles.train",'r')
or_trainlist=t1.readlines()
t1.close()

tnorm=open("articlesNorm.train",'w')

for line in or_trainlist:
    xsqr_tot=0
    
    xlist=line.strip("\n").split(" ")
    class_x=xlist.pop(0)
    x=[]
    f=[]
    
    for item in xlist:
        d=item.split(":")
        x.append(float(d[1]))
        xsqr_tot+=(float(d[1])**2)
        f.append(d[0])
    for p in range(len(xlist)):
        x[p]/=math.sqrt(xsqr_tot)

    tnorm.write(class_x)

    for p in range(len(xlist)):
        tnorm.write(" "+str(f[p])+":"+str(x[p]))

    tnorm.write("\n")

tnorm.close()

t1=open("articles.test",'r')
or_trainlist=t1.readlines()
t1.close()

tnorm=open("articlesNorm.test",'w')

for line in or_trainlist:
    xsqr_tot=0
    
    xlist=line.strip("\n").split(" ")
    class_x=xlist.pop(0)
    x=[]
    f=[]
    
    for item in xlist:
        d=item.split(":")
        x.append(float(d[1]))
        xsqr_tot+=(float(d[1])**2)
        f.append(d[0])
    for p in range(len(xlist)):
        x[p]/=math.sqrt(xsqr_tot)
        

    tnorm.write(class_x)
    for p in range(len(xlist)):
        tnorm.write(" "+str(f[p])+":"+str(x[p]))
    tnorm.write("\n")

tnorm.close()

file_name="articlesNorm.train"
file_test="articlesNorm.test"

carr=[]

c1=0.125

while(c1<=512):
    carr.append(c1)
    c1=c1*2

for c2 in carr:
    c=str(c2)
    i=1
    
    while i<=4:
        file_class_op="articles"+str(i)+".train"
        pred="Predfile_"+str(c)+"_"+str(i)
        t=open(file_class_op,"w")

        for line in (open(file_name, 'r')):
            s=list(line.strip("\n"))
            if s[0]==str(i):
                s[0]="1"
            else:
                s[0]="1"
                s.insert(0,"-")
            s.append("\n")
            line2="".join(s)  
            t.write(line2)
        t.close()
        i=i+1
        model_file="model_"+c+"_"+str(i-1)
        call(["svm_learn.exe", "-c", c, file_class_op, model_file])
        call(["svm_classify.exe",file_test,model_file,pred])

        predtrain="predfile_train_"+c+"_"+str(i-1)
        call(["svm_classify.exe",file_class_op,model_file,predtrain])

        t5=open(predtrain,"r")

        testlist=(t5.readlines())

        tr=open(file_class_op,'r')
        trainlist=tr.readlines()
    
        ctr=0
        k=0
        for line in trainlist:
            L=line.strip("\n").split(" ")
            al=testlist[k].strip("\n").split(" ")
            a=float(al[0])
            a=a/math.fabs(a)
        
            if int(L[0])!=int(a):
                ctr+=1
            k=k+1

        print "Class ",str(i-1)," ",ctr," C = ",c
        t5.close()
        tr.close()
