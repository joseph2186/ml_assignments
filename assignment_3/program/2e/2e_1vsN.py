import math

carr=[]
c1=0.125

while(c1<=512):
    carr.append(c1)
    c1=c1*2

for c2 in carr:
    c=str(c2)

    file_1="Predfile_"+c+"_1"
    file_2="Predfile_"+c+"_2"
    file_3="Predfile_"+c+"_3"
    file_4="Predfile_"+c+"_4"

    t=[]
    t.append(open(file_1,'r'))
    t.append(open(file_2,'r'))
    t.append(open(file_3,'r'))
    t.append(open(file_4,'r'))

    t5=open("Predictions_"+c,"w")

    testlist=(open("articlesNorm.test",'r').readlines())

    l=[]
    for i in range(4):
        l.append(t[i].readlines())

    testlen=len(l[0])
    x=[0,0,0,0]

    ctr=0

    for i in range(testlen):
        a=[]
        for j in range(4):
            a.append(float(l[j][i].strip(" ").strip("\n")))
        maxpos=0
        maxval=-99999.00
        for k in range(4):
            if(maxval<a[k]):
                maxval=a[k]
                maxpos=k+1

        tlist=testlist[i].split()
        if(int(tlist[0])!=maxpos):
            ctr=ctr+1
            x[int(tlist[0])-1]+=1

    for i in range(4):
        t[i].close()
    t5.close()

    print ctr," C = ",c
    print x," C = ",c
