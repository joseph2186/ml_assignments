from subprocess import call
import math
import random

t1=open("articles.train",'r')
or_trainlist=t1.readlines()
t1.close()

tnorm=open("articlesNorm.train",'w')
for line in or_trainlist:
    xsqr_tot=0
    
    xlist=line.strip("\n").split(" ")
    class_x=xlist.pop(0)
    x=[]
    f=[]
    
    for item in xlist:
        d=item.split(":")
        x.append(float(d[1]))
        xsqr_tot+=(float(d[1])**2)
        f.append(d[0])
    for p in range(len(xlist)):
        x[p]/=math.sqrt(xsqr_tot)

    tnorm.write(class_x)

    for p in range(len(xlist)):
        tnorm.write(" "+str(f[p])+":"+str(x[p]))

    tnorm.write("\n")

tnorm.close()

tnorm=open("articlesNorm.train",'r')
or_trainlist=tnorm.readlines()
tnorm.close()

x=int(0.75*len(or_trainlist))
y=len(or_trainlist)-x

shuffledlist=random.shuffle(or_trainlist)

train_file="articles_75.train"
val_file="articles_25.validate"

t=open(train_file,'w')
v=open(val_file,'w')

i=0

for line in or_trainlist:
    if i<x:
        t.write(line)
    else:
        v.write(line)
    i=i+1

t.close()
v.close()

carr=[]
c1=0.125

while(c1<=512):
    carr.append(c1)
    c1=c1*2

file_test=val_file
file_name=train_file

tnorm=open(file_name,'r')
or_trainlist=tnorm.readlines()
tnorm.close()

for c2 in carr:
    c=str(c2)
    for i in range(1,4):
        for j in range(i+1,5):

            list1=[]
            list2=[]
            for Line1 in or_trainlist:
                xlist=Line1.strip("\n").split(" ")
                class_x=int(xlist.pop(0))
                if class_x==i:
                    list1.append(Line1)
                if class_x==j:
                    list2.append(Line1)
            
            article_combo="articles_"+str(i)+str(j)+".train"
            tpred=open(article_combo,'w')

            for line in list1:
                xlist=line.strip("\n").split(" ")
                class_x=xlist.pop(0)
            
                tpred.write(str(1))
            
                for p in range(len(xlist)):
                    tpred.write(" "+xlist[p])

                tpred.write("\n")
        
            for line in list2:
                xlist=line.strip("\n").split(" ")
                class_x=xlist.pop(0)
                tpred.write(str(-1))

                for p in range(len(xlist)):
                    tpred.write(" "+xlist[p])

                tpred.write("\n")
        
            tpred.close()

    for i in range(1,4):
        for j in range(i+1,5):
            file_class_op="articles_"+str(i)+str(j)+".train"
            model_file="model_"+str(c)+"_"+str(i)+"_"+str(j)
            call(["svm_learn.exe", "-c", str(c), file_class_op, model_file])
            predtest="pred_"+str(c)+"_"+str(i)+"_"+str(j)
            call(["svm_classify.exe",file_test,model_file,predtest])

    t=[]
    k=0
    for i in range(1,4):
        for j in range(i+1,5):
            predtest="pred_"+str(c)+"_"+str(i)+"_"+str(j)
            t.append(open(predtest,'r'))

    tL=open(file_test,'r')
    testlist=tL.readlines()
    tL.close()
    totalacc=0.0
    for line in testlist:
        ctr=[0,0,0,0]

        lines6=[]
        for k in range(len(t)):
            cor_line=t[k].readline()
            lines6.append(float(cor_line.split(" ")[0]))

        predclass=[]
        k=0
        for i in range(1,4):
            for j in range(i+1,5):
                if(lines6[k]<0):
                    predclass.append(j)
                else:
                    predclass.append(i)
                k=k+1

        for prediction in predclass:
            ctr[prediction-1]+=1
        maxctr=-1
        
        for val in ctr:
            if maxctr<val:
                maxctr=val
                
        multiclass_pred=[]
        for y in range(len(ctr)):
            if maxctr==ctr[y]:
                multiclass_pred.append(y+1)
        
        actualclass=int(line.split(" ")[0])
        
	if actualclass in multiclass_pred:
            if len(multiclass_pred)==1:
                totalacc+=1.0
            elif len(multiclass_pred)==2:
                totalacc+=0.5
            else:
                totalacc+=(1.0/3.0)
    print totalacc, "C =",c
