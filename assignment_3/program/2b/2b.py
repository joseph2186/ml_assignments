from subprocess import call
import math
import random

t1=open("articles.train",'r')
or_trainlist=t1.readlines()
t1.close()

x=int(0.75*len(or_trainlist))
y=len(or_trainlist)-x

shuffledlist=random.shuffle(or_trainlist)

train_file="articles_75.train"
val_file="articles_25.validate"

t=open(train_file,'w')
v=open(val_file,'w')

i=0

for line in or_trainlist:
	if i<x:
		t.write(line)
	else:
		v.write(line)
	i=i+1

t.close()
v.close()

file_name=train_file
file_test=val_file

carr=[]
c1=0.125

while(c1<=512):
	carr.append(c1)
	c1=c1*2

for c2 in carr:
	i=1
	c=str(c2)
	while i<=4:
        	file_class_op=str(c)+"__articles"+str(i)+".train"
        	pred="Predfile_"+str(c)+"_"+str(i)
        	t=open(file_class_op,"w")


        	for line in (open(file_name, 'r')):
            		s=list(line.strip("\n"))
            		if s[0]==str(i):
                		s[0]="1"
            		else:
                		s[0]="1"
                		s.insert(0,"-")
            		s.append("\n")
            		line2="".join(s)  
            		t.write(line2)
        	t.close()
        	i=i+1
        	model_file="model_"+str(c)+"_"+str(i-1)
        	call(["svm_learn.exe", "-c", str(c), file_class_op, model_file])
        	call(["svm_classify.exe",file_test,model_file,pred])

        	predtrain="predfile_train_"+str(c)+"_"+str(i-1)
        	call(["svm_classify.exe",file_class_op,model_file,predtrain])

        	t5=open(predtrain,"r")
        	testlist=(t5.readlines())

        	tr=open(file_class_op,'r')
        	trainlist=tr.readlines()
                ctr=0
        	k=0
        	for line in trainlist:
            		L=line.strip("\n").split(" ")
            		al=testlist[k].strip("\n").split(" ")
            		a=float(al[0])
            		a=a/math.fabs(a)
        
            		if int(L[0])!=int(a):
                		ctr+=1
            		k=k+1

        	print "Class ",str(i-1)," ",ctr,"C =",str(c)
        	t5.close()
        	tr.close()

    
	file_1="Predfile_"+c+"_1"
    	file_2="Predfile_"+c+"_2"
    	file_3="Predfile_"+c+"_3"
    	file_4="Predfile_"+c+"_4"

    	t=[]
    	t.append(open(file_1,'r'))
    	t.append(open(file_2,'r'))
    	t.append(open(file_3,'r'))
    	t.append(open(file_4,'r'))

    	t5=open("Predictions_"+c,"w")

    	testlist=(open(val_file,'r').readlines())

    	l=[]
    	for i in range(4):
        	l.append(t[i].readlines())

    	testlen=len(l[0])
    	x=[0,0,0,0]

    	ctr=0

    	for i in range(testlen):
        	a=[]
        	for j in range(4):
            		a.append(float(l[j][i].strip(" ").strip("\n")))
        		maxpos=0
        		maxval=-99999.00
        		for k in range(4):
            			if(maxval<a[k]):
                			maxval=a[k]
                			maxpos=k+1

		tlist=testlist[i].split()
        	if(int(tlist[0])!=maxpos):
            		ctr=ctr+1
            		x[int(tlist[0])-1]+=1

	for i in range(4):
        	t[i].close()
    	t5.close()

    	print ctr,"C =",c
    	print x,"C =",c

