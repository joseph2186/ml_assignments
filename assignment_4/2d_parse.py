from subprocess import call
import sys
import operator

input_list = sys.argv
if len(sys.argv) > 1:
	print "calulating the accuracy for test with default c"
	train_file = "Data/webspam_train.svm"
	test_file = "Data/webspam_test.svm"
	result_file = "test_data/2d/result_data_for_default_c_test"
	model_file = "test_data/2d/learn_data_for_default_c_test"
	call(["svm_light_windows/svm_learn.exe", "-j", "0.1", train_file, model_file])
	call(["svm_light_windows/svm_classify.exe", test_file, model_file,  result_file])
	
	test_file = open(test_file, 'r')
	validate_file = open(result_file, 'r')
	output_compare_line = []
	false_positive_count = 0
	false_negative_count = 0
	count = 0

	for lines in test_file:
		output_compare_line.append(lines.split(" "))
		count += 1
	k = 0
	for lines in validate_file:
		if float(lines.strip("\n")) < 0 and float(output_compare_line[k][0]) > 0:
			false_negative_count += 1
		elif float(lines.strip("\n")) > 0 and float(output_compare_line[k][0]) < 0:
			false_positive_count += 1
		k += 1
	print "false_negative = "+str(false_negative_count)
	print "false positive = "+str(false_positive_count)
	print "accuracy = "+str(1 - float(false_positive_count + false_negative_count)/count)

else:
	k = 5
	c_list = (0.0001, 0.001, 0.01, 0.1, 1, 5, 10)

	false_positive = []
	false_negative = []
	total_line = []

	for i in range(k):
		output_compare_line = []
		count = 0
		validate_file = "test_data/2d/webspam_5fold_validate"+str(i)
		#print validate_file
		validate_file_instance = open(validate_file, 'r')
		for lines in validate_file_instance:
			output_compare_line.append(lines.split(" "))
			count += 1;
		total_line.append(output_compare_line)
		false_positive_dict = {}
		false_negative_dict = {}
		for j in c_list:
			false_positive_count = 0
			false_negative_count = 0
			result_file = "test_data/2d/result_data_for_c_"+str(j)+"_"+str(i)
			result_file_instance = open(result_file, 'r')
			#print result_file
			k = 0
			for line in result_file_instance:
				if float(line.strip("\n")) < 0 and float(output_compare_line[k][0]) > 0:
					false_negative_count += 1
				elif float(line.strip("\n")) > 0 and float(output_compare_line[k][0]) < 0:
					false_positive_count += 1
				k += 1
			false_positive_dict[j] = false_positive_count
			false_negative_dict[j] = false_negative_count
		false_positive.append(false_positive_dict)
		false_negative.append(false_negative_dict)

	#print false_positive
	#print false_negative

	result_positive_dict = {}
	result_negative_dict = {}
	result_dict = {}

	for j in c_list:
		positive = 0
		negative = 0
		for i in range(5):
			positive += false_positive[i][j]
			negative += false_negative[i][j]
		positive = float(positive)/5
		negative = float(negative)/5
		result_positive_dict[j] = positive
		result_negative_dict[j] = negative
		result_dict[j] = 1 - float(positive+negative)/len(total_line[i])

	print "Avg false positive = "+str(result_positive_dict)
	print "Avg false negative = "+str(result_negative_dict)
	print "Accuracy: "+str(result_dict)

				 
	best_c = max(result_dict.iteritems(), key=operator.itemgetter(1))[0]
	
	print "calulating the accuracy for test with best c = "+str(best_c)
	train_file = "Data/webspam_train.svm"
	test_file = "Data/webspam_test.svm"
	result_file = "test_data/2d/result_data_for_best_c_"+str(best_c)+"_test"
	model_file = "test_data/2d/learn_data_for_best_c_"+str(best_c)+"_test"
	call(["svm_light_windows/svm_learn.exe", "-c", str(best_c), "-j", "0.1", train_file, model_file])
	call(["svm_light_windows/svm_classify.exe", test_file, model_file,  result_file])

	test_file_instance = open(test_file, 'r')
	result_file_instance = open(result_file, 'r')
	output_compare_line = []
	false_positive = 0
	false_negative = 0
	count = 0

	for lines in test_file_instance:
		output_compare_line.append(lines.split(" "))
		count += 1
	k = 0
	for lines in result_file_instance:
		if float(lines.strip("\n")) < 0 and float(output_compare_line[k][0]) > 0:
			false_negative += 1
		elif float(lines.strip("\n")) > 0 and float(output_compare_line[k][0]) < 0:
			false_positive += 1
		k += 1
	print "false_negative = "+str(false_negative)
	print "false positive = "+str(false_positive)
	print "accuracy = "+str(1 - float(false_positive + false_negative)/count)

