import re
import math

train_file = open("Data/webspam_train.svm", 'r')
test_file = open("Data/webspam_test.svm", 'r')

#parse the lines to get the features
line_list = []
final_list = []
output_list = []
test_line_list = []
test_final_list = []
test_output_list = []
count_y_pos = 0
count_y_neg = 0
feature_list = []
test_feature_list = []

#parsing the data of the svm input data files
for lines in train_file:
	m = re.compile('[:]')
	temp = m.split(lines.strip(" "))
	line_list.append(m.split(lines.strip("\n")))

for i in range(len(line_list)):
	temp_list = []
	sub_list = []
	j = 0
	for temp in line_list[i]:
		temp_list.append(str(temp).split(" "))
		if len(temp_list[j]) > 1:
			sub_list.append(int(temp_list[j][1]))
			feature_list.append(int(temp_list[j][1]))
		j += 1
	output_list.append(temp_list[0][0])
	final_list.append(sub_list)

#calculate all the data required for the probability calculation
feature_list = list(set(feature_list))
feature_list.sort()
no_of_features = max(feature_list)
no_of_docs = len(output_list)

#parse the data of the svm test file
for lines in test_file:
	m = re.compile('[:]')
	temp = m.split(lines.strip(" "))
	test_line_list.append(m.split(lines.strip("\n")))

for i in range(len(test_line_list)):
	temp_list = []
	sub_list = []
	j = 0
	for temp in test_line_list[i]:
		temp_list.append(str(temp).split(" "))
		if len(temp_list[j]) > 1:
			sub_list.append(int(temp_list[j][1]))
			test_feature_list.append(int(temp_list[j][1]))
		j += 1
	test_output_list.append(temp_list[0][0])
	test_final_list.append(sub_list)

feature_dict_positive = {}
feature_dict_negative = {}
test_feature_dict_positive = {}
test_feature_dict_negative = {}

#populate the feature values to 0 to initialize
for i in feature_list:
	feature_dict_negative[i] = 0
	feature_dict_positive[i] = 0

for i in feature_list:
	test_feature_dict_negative[i] = 0
	test_feature_dict_positive[i] = 0

flag_pos = 0
flag_neg = 0
#get the count of the number of docs in both classes and increment the feature values
for i in feature_list:
	for j in range(no_of_docs):
		if output_list[j] == "+1":
			#positive docs
			if flag_pos == 0:
				count_y_pos += 1
			if i in final_list[j]:
				feature_dict_positive[i] += 1
		else:
			if flag_neg == 0:
				count_y_neg += 1
			if i in final_list[j]:
				feature_dict_negative[i] += 1
	#just a flag to count only in one loop
	flag_pos = 1
	flag_neg = 1

flag_pos = 0
flag_neg = 0
test_count_y_pos = 0
test_count_y_neg = 0
#get the count of the number of docs in both classes and increment the feature values
for i in feature_list:
	for j in range(len(test_output_list)):
		if test_output_list[j] == "+1":
			#positive docs
			if flag_pos == 0:
				test_count_y_pos += 1
			if i in final_list[j]:
				test_feature_dict_positive[i] += 1
		else:
			if flag_neg == 0:
				test_count_y_neg += 1
			if i in final_list[j]:
				test_feature_dict_negative[i] += 1
	#just a flag to count only in one loop
	flag_pos = 1
	flag_neg = 1
'''
print no_of_docs
print no_of_features
print count_y_pos
print count_y_neg
print len(feature_list)
print feature_list
print feature_dict_positive
print feature_dict_negative
'''
#keep track of which class has more documents
max_count = 0
if (count_y_pos > count_y_neg):
	max_count = "1"
else:
	max_count = "-1"

#calculate P(Y)
P_y_pos = float(count_y_pos)/no_of_docs
P_y_neg = float(count_y_neg)/no_of_docs

test_classify_output = []
#classify test data
#take one document at a time
for i in range(len(test_output_list)):
	feature_sum_pos = 0
	feature_sum_neg = 0
	#for all the features from 1-the max present in the train data
	for j in feature_list:
		#is the feature present in the document 
		if j in test_final_list[i]:
			#check if the feature is present in any document from class y=+1 or not
			if feature_dict_positive[j] == 0:
				temp_sum_pos = 1
			else:
				#the feature is present in the document
				temp_sum_pos = float(feature_dict_positive[j])/count_y_pos
			#check if the feature is present in any document from class y=-1 or not
			if feature_dict_negative[j] == 0:
				temp_sum_neg = 1
			else:
				temp_sum_neg = float(feature_dict_negative[j])/count_y_neg
			#summation of the feature probability of presence in a document of a type of class
			feature_sum_pos += math.log(temp_sum_pos)
			feature_sum_neg += math.log(temp_sum_neg)
		else:
			#check if the feature is present in any document from class y=+1 or not
			if feature_dict_positive[j] == 0:
				temp_sum_pos = 0
			else:
				temp_sum_pos = float(feature_dict_positive[j])/count_y_pos
			#check if the feature is present in any document from class y=-1 or not
			if feature_dict_negative[j] == 0:
				temp_sum_neg = 0
			else:
				temp_sum_neg = float(feature_dict_negative[j])/count_y_neg
			
			if temp_sum_pos == 1:
				feature_sum_pos += 1
			else:
				feature_sum_pos += math.log(1-temp_sum_pos)
			if temp_sum_neg == 1:
				feature_sum_neg += 1
			else:
				feature_sum_neg += math.log(1-temp_sum_neg)
	#add the values of the P(Y=y)
	feature_sum_pos += math.log(P_y_pos)
	feature_sum_neg += math.log(P_y_neg)

	#find the max
	if (feature_sum_pos == feature_sum_neg):
		test_classify_output.append(max_count)
	elif (feature_sum_pos > feature_sum_neg):
		test_classify_output.append("+1")
	else:
		test_classify_output.append("-1")

false_positive = 0
false_negative = 0
for i in range(len(test_classify_output)):
	if test_classify_output[i] != test_output_list[i]:
		if test_classify_output[i] == "-1":
			false_negative += 1
		else:
			false_positive += 1

'''
print "Actual Values:"
print "False negative = "+str(false_negative)
print "False positive = "+str(false_positive)
'''
print "Actual error rate  = "+str(float(false_positive + false_negative)/len(test_classify_output))

denominator = 0
result_pos = 0
result_neg = 0
result = 0
feature_product_pos = 1
feature_product_neg = 1
#go through each document in the test file
for i in range(len(test_final_list)):
	feature_product_pos = 1
	feature_product_neg = 1
	#for each of the feature
	for j in feature_list:
		#check if the feature is present in the document
		if j in test_final_list[i]:
			feature_product_pos *= float(feature_dict_positive[j])/count_y_pos
			feature_product_neg *= float(feature_dict_negative[j])/count_y_neg
		#the feature is not present in the document
		else:
			#check if the feature is present in any document from class y=+1 or not
			feature_product_pos *= 1 - float(feature_dict_positive[j])/count_y_pos
			feature_product_neg *= 1 - float(feature_dict_negative[j])/count_y_neg
	'''
	if feature_product_pos == 0 and feature_product_neg == 0:
		print "feature_product_pos = "+str(feature_product_pos)
		print "feature_product_neg = "+str(feature_product_neg)
		print "p_y_pos = "+str(P_y_pos)
		print "p_y_neg = "+str(P_y_neg)
	'''
	if feature_product_pos != 0 or feature_product_neg != 0:
		denominator = P_y_pos * feature_product_pos + P_y_neg * feature_product_neg
		result_pos = 1 - (float(P_y_pos * feature_product_pos) / denominator)
		result_neg = 1 - (float(P_y_neg * feature_product_neg) / denominator)
	if (result_pos < result_neg):
		result += result_pos
	else:
		result += result_neg

result = float(result)/len(test_classify_output)
print "Theoritical error = "+str(result)
