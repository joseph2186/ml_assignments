from subprocess import call
import random

orig_file_train = open("test_data/webspam_train.svm", 'r')
data_len = 0
lines = []
for line in orig_file_train:
	data_len += 1;
	lines.append(line)

#Create the list with 5 split indices in the training data
split_index_list = []
split_value = data_len/5
split_index_list.append(0)
for i in range(4):
	split_index_list.append((i+1)*split_value)
split_index_list.append(data_len)

k = len(split_index_list)
print split_index_list

i=0
j = 0
for i in range(k-1):
	j = k-i-1
	file_train_name = "test_data/webspam_5fold_train"+str(i)
	file_validate_name = "test_data/webspam_5fold_validate"+str(i)
	file_train = open(file_train_name, 'w')
	file_validate = open(file_validate_name, 'w')
	print split_index_list[j-1]
	print split_index_list[j]
	for n in range(split_index_list[j-1], split_index_list[j]):
		file_validate.write("%s" % lines[n])
	for l in range(k-1):
		if l+1 != j:
			print split_index_list[l]
			print split_index_list[l+1]
			for m in range(split_index_list[l], split_index_list[l+1]):
				file_train.write("%s" % lines[m])
	file_train.close()
	file_validate.close()

		

for i in range(k-1):
	file_train_name = "test_data/webspam_5fold_train"+str(i)
	file_validate_name = "test_data/webspam_5fold_validate"+str(i)
	learnt_file = "test_data/learnt_data_for_c_default"+"_"+str(i)
	result_file = "test_data/result_data_for_c_default"+"_"+str(i)
	call(["svm_light_windows/svm_learn.exe","-j", "0.1", file_train_name, learnt_file])
	call(["svm_light_windows/svm_classify.exe", file_validate_name, learnt_file, result_file])

c_list = (0.0001, 0.001, 0.01, 0.1, 1, 5, 10)
for c in c_list:
	for i in range(k-1):
		file_train_name = "test_data/webspam_5fold_train"+str(i)
		file_validate_name = "test_data/webspam_5fold_validate"+str(i)
		learnt_file = "test_data/learnt_data_for_c_"+str(c)+"_"+str(i)
		result_file = "test_data/result_data_for_c_"+str(c)+"_"+str(i)
		print learnt_file
		call(["svm_light_windows/svm_learn.exe", "-c", str(c), "-j", "0.1", file_train_name, learnt_file])
		print result_file
		call(["svm_light_windows/svm_classify.exe", file_validate_name, learnt_file,  result_file])

