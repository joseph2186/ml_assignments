import sys
import re
import math
import copy
import shutil
import random

#Variable to enable debug
debug = 0
#debug print api
def debug_print(obj):
	if debug == 1:
		print obj

class tree_node(object):
	left_obj = None
	right_obj = None
	def __init__(self, is_leaf, word_list, router_list, left_obj, right_obj, entropy, threshold, output, router, level):
		self.is_leaf = is_leaf
		word_list_temp = {}
		if word_list != None:
			self.word_list = []
		if word_list != None:
			for i in range(len(word_list)):
				for j in range(len(word_list[i])):
					word_list_temp = copy.deepcopy(word_list[i])
				self.word_list.append(word_list_temp)
		if router_list != None:
			self.router_list = router_list[:]
		self.left_obj = copy.deepcopy(left_obj)
		self.right_obj = copy.deepcopy(right_obj)
		self.entropy = entropy
		self.threshold = threshold
		self.output = output
		self.router = router
		self.level = level

#Function to calculate the entropy given the values in a dict
def entropy(dict_d):
	e=0
	sum_val = sum(dict_d.values())
	for item in dict_d.keys():
		if dict_d[item] != 0:
			#p = ni/N
			p=float(dict_d[item])/sum_val
			if p!=0:
				#entropy = e - (sum(p*log2p))
				e=e-p*math.log(p)/math.log(2)
	return e
max_level = 0

output_list = ["ATRIUM_1", "ATRIUM_2", "HALLWAY", "LOUNGE", "MATTINS", "PHILLIPS"]

#Function to be called recursively to get the hypothesis
def nodeproc(node):
	global max_level
	if max_level < node.level:
		max_level = node.level
	#List of all output values
	
	#Counter used to calculate the entropy at the root
	counter=[0]*6
	for i in range(len(node.word_list)):
		counter[output_list.index(node.word_list[i][10])] += 1
	
	#List that stores the unique attribute values for a given tuple
	#Here the attribute is the router and the value is the signal strength
	list_router = []
	for i in range(10):
		list_temp = []
		for j in range(len(node.word_list)):
			if int(node.word_list[j][i]) not in list_temp:
				list_temp.append(int(node.word_list[j][i]))
		list_temp.sort()
		list_router.append(list_temp)

	#Logic to calculate the entropy at the root
	entropy_root=0
	for i in range(len(output_list)):
		if (len(node.word_list)) != 0:
			p=float(counter[i])/len(node.word_list)
			if p!=0:
				entropy_root -= p*math.log(p)/math.log(2)
	
	#Counters that will store the count of the attributes for a given threshold
	#initialize them to 0
	left_counter = {}
	right_counter = {}
	for i in range(len(output_list)):
		left_counter[output_list[i]] = 0
		right_counter[output_list[i]] = 0
	
	max_info_gain = -1
	router = 99
	threshold = 0

	#Loop through all the attributes present
	for i in node.router_list:
		#Loop through the values for each attribute
		for j in range(len(list_router[i])):
			root_threshold = int(list_router[i][j])
		
			#logic to set all the values to zero - check for a pythonic way
			for k in left_counter.keys():
				left_counter[k]=0
				right_counter[k]=0
			
			#Loop through each tuple
			for l in range(len(node.word_list)):
				if int(node.word_list[l][i]) <= root_threshold:
					left_counter[node.word_list[l][10]] += 1
				else:
					right_counter[node.word_list[l][10]] += 1
			
			left_entropy=entropy(left_counter)
			right_entropy=entropy(right_counter)
			
			sum_left = 0
			sum_right = 0
			
			for m in left_counter.keys():
				sum_left += left_counter[m]
				sum_right += right_counter[m]
			
			info_gain = entropy_root - ((float(sum_left)/(sum_left + sum_right) * left_entropy) + (float(sum_right) / (sum_left + sum_right) * right_entropy))
			#logic to store the maximum information gain
			if info_gain > max_info_gain:
				max_info_gain=info_gain
				router=i
				threshold=root_threshold
	#update the root node
	node.threshold = threshold
	node.entropy = entropy_root
	node.router = router

	#figure if a node created is pure
	
	#Recursion
	#If the router is the attribute to divide on, delete it from the 
	#router list
	router_list_local = node.router_list[:]
	
	#Exit condition: If there is only one populated value in counter
	#and that value is equal to the size of the word_list then there
	#is only one attribute with non-zero value - HENCE PURE
	if max_info_gain == 0:
		if len(node.word_list) in counter:
			node.is_leaf = 1
			pos=counter.index(len(node.word_list))
			node.output = output_list[pos]

		else:
			#Classify by Max valued output...
			node.is_leaf = 1
			max_item=output_list[counter.index(max(counter))]
			node.output = max_item
	else:
		
		if len(node.router_list)!=0:
			#Split and call L and R
			left_node = tree_node(0, None, node.router_list, None, None, 0, 0, "0", 0, node.level + 1)
			right_node = tree_node(0, None, node.router_list, None, None, 0, 0, "0", 0, node.level + 1)
			node.left_obj = left_node
			node.right_obj = right_node
			WLLeft=[]
			WLRight=[]
			for i in range(len(node.word_list)):
				if node.word_list[i][router]<=threshold:
					WLLeft.append(node.word_list[i])
				else:
					WLRight.append(node.word_list[i])
			
			left_node.word_list = copy.deepcopy(WLLeft)
			nodeproc(left_node)
			
			right_node.word_list = copy.deepcopy(WLRight)
			nodeproc(right_node)
			
level_dict = {}
def traverse_tree(node):
	if node.is_leaf == 1:
		level_dict[node.level].append(node.output)
		print node.level
	else:
		if node.is_leaf != 1:
			level_dict[node.level].append(node.router)
		print node.level
		traverse_tree(node.left_obj)
		traverse_tree(node.right_obj)

def display_tree():
	for i in range(len(level_dict)):
		print level_dict[i]


#setting the tree to a certain level
def tree_level(root_node, level):
	if root_node.level == level:
		counter=[0]*6
		for i in range(len(root_node.word_list)):
			counter[output_list.index(root_node.word_list[i][10])] += 1
		if len(root_node.word_list) in counter:
			root_node.is_leaf = 1
			pos=counter.index(len(root_node.word_list))
			root_node.output = output_list[pos]

		else:
			#Classify by Max valued output...
			root_node.is_leaf = 1
			max_item=output_list[counter.index(max(counter))]
			root_node.output = max_item
	else:
		if (root_node.left_obj != None):
			if (root_node.right_obj != None):
				tree_level(root_node.left_obj, level)
				tree_level(root_node.right_obj, level)
			else:
				tree_level(root_node.left_obj, level)
		else:
			if (root_node.right_obj != None):
				tree_level(root_node.right_obj, level)

def read_data(file_name):
	word_list = []
	for line in (open(file_name, 'r')):
		m = re.compile(':* *')
		line1 = m.split(line.strip("\n").strip("\r"))
		temp_dict = {}
		for i in range(10):
			if str(i) in line1:
				temp_dict[i] = int(line1[line1.index(str(i)) + 1])
			else:
				temp_dict[i] = -999
		temp_dict[10] = line1[-1]
		word_list.append(temp_dict)

	debug_print(word_list)
	return word_list

def test_hypothesis(root_node, test_list):
	node = root_node
	while node.is_leaf != 1:
		if test_list[int(node.router)] <= node.threshold:
			node = node.left_obj
		else:
			node = node.right_obj
	return node

def mcnemar_test(d1, d2):
	pr_d1 = 0
	pr_d2 = 0
	k = d1+d2
	for i in range(d1+1):
		pr_d1 += float(math.factorial(k))/(math.factorial(i) * math.factorial(k-i)) * (0.5**(k))
	for i in range(d2+1):
		pr_d2 += float(math.factorial(k))/(math.factorial(i) * math.factorial(k-i)) * (0.5**(k))

	print "Pr_d1: ", pr_d1, "for d1 =", d1
	print "Pr_d2: ", pr_d2, "for d2 =", d2

	x_delta = pow((math.fabs(d2-d1) - 1), 2)/k

	print "x_delta = ", x_delta
	print "since x_delta", x_delta, " > 3.841459, this hypothesis holds"
	
	if pr_d1 < pr_d2:
		print "Since Pr_d1 is less than 2.5%"
		return d1
	else:
		print "Since Pr_d2 is less than 2.5%"
		return d2

def ttest(d1, d2):
	#find the difference between the 2
	di = []
	da = 0
	sdi = 0
	
	for i in range(len(d1)):
		#di.append(math.fabs(d1[i]-d2[i]))
		di.append(d2[i]-d1[i])
	
	for j in range(len(d1)):
		da += di[j]
	
	d_mean = da/10

	print "Mean =", d_mean

	for k in range(len(d1)):
		sdi += pow((di[k] - d_mean), 2)
	
	sdi_k = sdi/90

	variance = sdi/9

	print "Variance =", variance

	sdi_final = pow(sdi_k, 0.5)

	tnk = (pow(10, 0.5) * d_mean)/variance

	print "T = ", tnk

	print "SD = ", sdi_final


train_file = "Training set.train"
test_file = "Testing set.test"

test_list = []

router_list=[0,1,2,3,4,5,6,7,8,9]

#Splitting begins.....
menu_input = raw_input("enter the question:")

if menu_input == "a":
	
	word_list = read_data(train_file)
	error_classes = []
	#Create the root node for the tree
	root_node = tree_node(0, word_list, router_list, None, None, 0, 0, "0", 0, 0) 
	nodeproc(root_node)

	counter=[0]*6
	error_counter=[0]*6
	sum_correct = 0
	for i in range(len(word_list)):
		node = root_node
		while node.is_leaf != 1:
			if word_list[i][int(node.router)] <= node.threshold:
				node = node.left_obj
			else:
				node = node.right_obj

		if node.output == word_list[i][10]:
			sum_correct += 1
		else:
			error_counter[output_list.index(word_list[i][10])] += 1

	for i in range(len(output_list)):
		for j in range(len(word_list)):
			counter[output_list.index(word_list[j][10])] += 1
	
	print "error rate for classes"
	for i in range(len(counter)):
		print output_list[i], "=", float(error_counter[output_list.index(output_list[i])])/counter[output_list.index(output_list[i])] * 100, "%"
	print "error rate on the entire training sample =", (1 - float(sum_correct)/len(word_list))*100, "%"

elif menu_input == "b":
	
	word_list = read_data(train_file)
	#Create the root node for the tree
	root_node = tree_node(0, word_list, router_list, None, None, 0, 0, "0", 0, 0) 
	nodeproc(root_node)
	
	counter=[0]*6
	error_counter=[0]*6
	sum_correct = 0
	test_list = read_data(test_file)
	for i in range(len(test_list)):
		node = test_hypothesis(root_node, test_list[i])
		if node.output == test_list[i][10]:
			sum_correct += 1
		else:
			error_counter[output_list.index(word_list[i][10])] += 1

	for i in range(len(output_list)):
		for j in range(len(word_list)):
			counter[output_list.index(word_list[j][10])] += 1
	
	print "error rate for classes"
	for i in range(len(counter)):
		print output_list[i], "=", float(error_counter[output_list.index(output_list[i])])/counter[output_list.index(output_list[i])] * 100, "%"
	print "error rate =", (1 - float(sum_correct)/len(test_list)) * 100, "%"

elif menu_input == "display":
	
	word_list = read_data(train_file)
	#Create the root node for the tree
	root_node = tree_node(0, word_list, router_list, None, None, 0, 0, "0", 0, 0) 
	nodeproc(root_node)

	#At the end of this operation, list_dict has all the nodes at their levels    
	for i in range(max_level+1):
		level_dict[i] = [] 
	traverse_tree(root_node)
	display_tree()

elif menu_input == "c":
	
	word_list = read_data(train_file)
	#Create the root node for the tree
	root_node = tree_node(0, word_list, router_list, None, None, 0, 0, "0", 0, 0) 
	nodeproc(root_node)

	d1 = 0
	d2 = 0

	level = raw_input("enter the level you want to stop at ")
	level = int(level)

	test_root = copy.deepcopy(root_node)
	tree_level(test_root, level)
	
	test_list = read_data(test_file)

	for i in range(len(test_list)):
		node_h1 = test_hypothesis(root_node, test_list[i])
		node_h2 = test_hypothesis(test_root, test_list[i])

		if node_h1.output != test_list[i][10]:
			if node_h2.output == test_list[i][10]:
				d1 += 1
		
		if node_h2.output != test_list[i][10]:
			if node_h1.output == test_list[i][10]:
				d2 += 1
	err_pr = mcnemar_test(d1, d2)

	#TODO: change the logic to make it look more logical
	if err_pr == d1:
		print "h1 is better"
	else:
		print "h2 is better"
	

elif menu_input == "d":
	concat_file = open("train+test", 'w')
	shutil.copyfileobj(open(train_file, 'rb'), concat_file)
	shutil.copyfileobj(open(test_file, 'rb'), concat_file)
	concat_file.close()
	
	combined_list = read_data("train+test")
	
	level = 2
	test_list = []
	j = 0
	for i in range(9):
		test_list.append(combined_list[j:j+48])
		j = j + 48
	test_list.append(combined_list[j:])
	#print test_list	
	
	d1_list = []
	for i in range(len(test_list)):
		true_test_list = []
		train_list = []
		for j in range(len(test_list)):
			if j != i:
				for k in range(len(test_list[j])):
					train_list.append(test_list[j][k])
		true_test_list = copy.deepcopy(test_list[i])
		#Create the root node for the tree
		root_node = tree_node(0, train_list, router_list, None, None, 0, 0, "0", 0, 0)
		nodeproc(root_node)
		d1 = 0
		for i in range(len(true_test_list)):
			node_h1 = test_hypothesis(root_node, true_test_list[i])
			if node_h1.output != true_test_list[i][10]:
				d1 += 1
		d1_list.append(d1)
		
	d2_list = []
	for i in range(len(test_list)):
		true_test_list = []
		train_list = []
		for j in range(len(test_list)):
			if j != i:
				for k in range(len(test_list[j])):
					train_list.append(test_list[j][k])
		true_test_list = copy.deepcopy(test_list[i])
		#Create the root node for the tree
		root_node = tree_node(0, train_list, router_list, None, None, 0, 0, "0", 0, 0)
		nodeproc(root_node)
		d2 = 0
		test_root = copy.deepcopy(root_node)
		tree_level(test_root, level)
		for i in range(len(true_test_list)):
			node_h2 = test_hypothesis(test_root, true_test_list[i])
			if node_h2.output != true_test_list[i][10]:
				d2 += 1
		d2_list.append(d2)
	print d1_list
	print d2_list
	ttest(d1_list, d2_list)	

elif menu_input == "e":	
	#Parse the training data
	word_list = read_data(train_file)
	test_list = read_data(test_file)

	random_list = []
	for i in range(len(word_list)):
		random_list.append(i)

	#Randomly shuffle the list indices
	random.shuffle(random_list)

	#training_list
	training_list = []

	#validation list
	validation_list =[]

	root_node = None
	node = None

	#error_list
	error_list = []

	#split the tuples in different ratios from 0.1 to 0.9
	ratio = 0

	#list of error in each ratio list
	errors_level = []

	#error_dict gives the best depth for each ratio split
	error_dict = {}
	
	while ratio <= 0.8:
		ratio += 0.1
		
		#training_list
		training_list = []

		#validation list
		validation_list =[]

		#populate the validation and the training set for the 
		#different ratios
		validation_index = int(len(random_list) * ratio)
		training_index = len(random_list) - validation_index

		for i in range(0, training_index):
			training_list.append(word_list[random_list[i]])
		
		for i in range(training_index, len(random_list)):
			validation_list.append(word_list[random_list[i]])

		#Build the tree with the training list
		root_node = None
		root_node = tree_node(0, training_list, router_list, None, None, 0, 0, "0", 0, 0)
		nodeproc(root_node)

		test_node = copy.deepcopy(root_node)
		#validate it at different levels
		#Find the total errors with different max levels
		error_count = 0
		error_list = []
		for j in range(len(validation_list)):
			node = test_hypothesis(test_node, validation_list[j])

			if node.output != validation_list[j][10]:
				error_count += 1
		error_list.append(error_count)
		for k in range(max_level):
			#init the node to default
			node = None
			test_node = None
			error_count = 0
			test_node = copy.deepcopy(root_node)
			tree_level(test_node, (max_level - 1) - k)

			#test the new tree with validation
			for l in range(len(validation_list)):
				node = test_hypothesis(test_node, validation_list[l])

				if node.output != validation_list[l][10]:
					error_count += 1
			error_list.append(error_count)
		errors_level.append(error_list)
	#store the optimal depth for each ratio division
	i = 0
	j = 0
	while(i <= 0.8):
		i += 0.1
		error_dict[float(i)] = max_level - errors_level[j].index(min(x for x in errors_level[j]))
		j += 1
	
	#Now run this nonsense on 10 different random trees
	#for each ratio 
	test_errors_level = []
	validation_errors_level = []
	for i in range(10):
		validation_error_list = []
		test_error_list = []
		validation_size = []
		ratio = 0
		while ratio <= 0.8:
			ratio += 0.1
			
			#Randomly shuffle the list indices
			random.shuffle(random_list)
			
			#training_list
			training_list = []

			#validation list
			validation_list =[]

			#populate the validation and the training set for the 
			#different ratios
			validation_index = int(len(random_list) * ratio)
			training_index = len(random_list) - validation_index

			for i in range(0, training_index):
				training_list.append(word_list[random_list[i]])
			
			for i in range(training_index, len(random_list)):
				validation_list.append(word_list[random_list[i]])

			#store the validation set size for each ratio split
			validation_size.append(len(validation_list))

			#Build the tree with the training list
			root_node = None
			root_node = tree_node(0, training_list, router_list, None, None, 0, 0, "0", 0, 0)
			nodeproc(root_node)

			test_node = copy.deepcopy(root_node)
			#validate it at different levels
			#Find the total errors with different max levels
			validation_error_count = 0
			test_error_count = 0
			
			#init the node to default
			node = None
			tree_level(test_node, error_dict[ratio])

			#test the new tree with validation
			for l in range(len(validation_list)):
				node = test_hypothesis(test_node, validation_list[l])
				if node.output != validation_list[l][10]:
					validation_error_count += 1
			validation_error_list.append(validation_error_count)
			#test the new tree with validation
			for l in range(len(test_list)):
				node = test_hypothesis(test_node, test_list[l])
				if node.output != test_list[l][10]:
					test_error_count += 1
			test_error_list.append(test_error_count)
		validation_errors_level.append(validation_error_list)
		test_errors_level.append(test_error_list)

	for i in range(len(validation_errors_level)):
		print "validation error for run", i+1
		for j in range(len(validation_errors_level[i])):
			sys.stdout.write(str(float(validation_errors_level[i][j])/validation_size[j]))
			sys.stdout.write(" ")
		sys.stdout.write("\n")
		print "test error for run", i+1
		for j in range(len(test_errors_level[i])):
			sys.stdout.write(str(float(test_errors_level[i][j])/len(test_list)))
			sys.stdout.write(" ")
		sys.stdout.write("\n")
